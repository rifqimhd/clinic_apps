<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/detail_dokter/{id}', function (String $id) {
    $data = DB::table('dokters')->where('id', $id)->first();
    return view('detail_dokter',compact('data'));
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/foto', [App\Http\Controllers\FotoController::class, 'index'])->name('foto');
Route::get('/dokter', [App\Http\Controllers\DokterController::class, 'index'])->name('dokter');

Route::get('/foto/delete/{id}', [App\Http\Controllers\FotoController::class, 'destroy']);
Route::get('/dokter/delete/{id}', [App\Http\Controllers\DokterController::class, 'destroy']);

Route::get('/foto/add', [App\Http\Controllers\FotoController::class, 'create']);

Route::post('/foto/store', [App\Http\Controllers\FotoController::class, 'store']);

Route::get('/dokter/add', [App\Http\Controllers\DokterController::class, 'create']);

Route::post('/dokter/store', [App\Http\Controllers\DokterController::class, 'store']);