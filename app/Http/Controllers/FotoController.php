<?php

namespace App\Http\Controllers;
use App\Models\Foto;

use Illuminate\Http\Request;

class FotoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Foto::cursorPaginate(5);
        return view('foto.view',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('foto.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
    // echo 'File Name: '.$file->getClientOriginalName();
    // echo 'File Real Path: '.$file->getRealPath();
    // echo 'File Size: '.$file->getSize();
    // echo 'File Mime Type: '.$file->getMimeType();
    // public_path('data/sample_foto.json') if manual

         $loc =  $request->file('data')->getPathName();
         $post  = file_get_contents($loc);
         $data = json_decode($post,true);
        
        for($i=0; $i < count($data); $i++){
            Foto::create([
                'name' => $data[$i]['name'],
                'path' => $data[$i]['path']
            ]);
        }

        return redirect('/foto');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Foto::findOrFail($id);
        $data->delete();
        return redirect('/foto');
    
    }

    public function list()
    {
        return Foto::orderBy('id', 'desc')->take(3)->get();
    }
}
