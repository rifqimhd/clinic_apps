<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ERHA</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <style>
        body {
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            font-size: 16px;
        }

        .pops{
            width:58%;
            height:auto;
            background-color:white;
            display:none;
            top:15px;
            left:0px;
            right:0px;
            position: fixed;
            z-index: 1;
            margin:auto;
        }

        @media screen and (max-width: 1200px) {
            #fr {
                display:none;
            }
        }

        @media screen and (max-width: 1000px) {
            #people{
                display:none;
            }
        }
    </style>
</head>

<body class="bg-light">
    <div class="container">

        <div class="row pops" id="popups">
            <div class="col-md-12">
                <p>
                    <input class="form-control w-100" type="search" placeholder="Search" aria-label="Search"> 
                </p><p>
                    <button class="btn btn-warning text-white" type="button">Search</button> <button class="btn btn-danger" type="button" onclick="closePopUp()">Close</button>
                </p>
            </div>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ asset('asset/logo.png') }}" width="80px" style="border-radius: 10%;" /></a>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0" style="margin-left: auto;">
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Solusi Sesuai Problem
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="https://erhaultimate.co.id/acne-center"><span class="text-warning">Acne Center</span> <br />
                                <small>Kulit Jerawat dan Bekas Jerawat</small> </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://erhaultimate.co.id/anti-aging-center"><span class="text-warning">Anti Aging Center</span> <br />
                                <small>Kulit Kendur dan Garis Halus</small></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://erhaultimate.co.id/brightening-center"><span class="text-warning">Brightening Center</span>
                                <br /> <small>Kulit Kusam dan Noda Hitam</small> </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://erhaultimate.co.id/hair-care-center"><span class="text-warning">Hair Care Center </span> <br />
                                <small>Rambut Rontok dan Botak</small> </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://erhaultimate.co.id/make-over-center"><span class="text-warning">Make Over Center </span> <br />
                                <small>Bentuk Wajah dan Kurang Ideal</small> </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://erhaultimate.co.id/atopy-and-skin-disease-center"><span class="text-warning">Atopy dan Skin & Disease Center
                                </span> <br /> <small>Dermatitis Atopik & Penyakit Kulit</small> </a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://erhaultimate.co.id/promo">Promo Spesial</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://erhaultimate.co.id/erha-story">Kisag ERHA</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://erhaultimate.co.id/article">Info Terkini</a>
                    </li>
                </ul>
                <form id="fr" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search" onfocus="munculPopUp()">
                    <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Search</button>
                    &nbsp;
                </form>
                <button id="people" class="btn btn-outline-warning my-2 my-sm-0" onclick="window.location.href='/login'">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-person" viewBox="0 0 16 16">
                            <path
                                d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z" />
                        </svg>
                    </button>
            </div>
        </nav>

        <div class="row text-center">
            <div class="col-lg-12 text-left" style="height: 50px; margin-top:30px; margin-left: 10px; cursor:pointer" onclick="window.location.href='/'">
            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="#f0ad4e" class="bi bi-arrow-left-circle" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8m15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0m-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5z"/>
            </svg>
            </div>
        </div>

        <br />

        <div class="row text-center">
            <div class="col-lg-12" style="height: 220px; margin-top:10px;">
                    <span class="mt-20">
                        <img src="{{$data->path}}"  width="120" alt="..." style="border-radius: 50%; "/>
                    </span>
                    <h5 class="font-weight-bold" >{{$data->name}}</h5>
                    <h6 class="text-warning">No STR 3121100220154463</h6>
                    <h6 class="text-warning"> 5 Years Experience </h6>
            </div>
        </div>

        <br />

        <div class="row text-left"
            style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;">
            <div class="col-lg-6 p-3 mt-20">
                <h5 class="font-weight-bold">Clinical Interest</h5>
                <h6> Acne, Brightening, Anti-Aging, Make Over, Hair </h6>

            </div>

            <div class="col-lg-6 p-3 mt-20">
                <h6 style="font-weight: bolder;">Lokasi Klinik</h6>
                <div class="dropdown">
                    <button class="btn btn-outline-warning dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Lokasi Klinik
                    </button>
                    <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Cimahi</a> <hr>
                        <a class="dropdown-item" href="#">Bandung - Soekarno Hatta</a>
                    </div>
                    </div>
            </div>
        </div>


        <div class="row text-left"
            style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;">
            <div class="col-lg-6 p-3 mt-20">
                <h5 class="font-weight-bold">Pendidikan</h5>
                <h6> Universitas Kristen Maranata : Pendidikan Dokter Umum </h6>

            </div>

            <div class="col-lg-6 p-3 mt-20">
            <ul class="list-group w-100">
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Senin </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>    
                </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Selasa </div>
                      <div class="col-md-6 text-right"> 09:00 - 19:00 </div>
                    </div>  
                </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Rabu </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>  
                </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Kamis </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Jumat </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>
                 </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left"> Sabtu </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                      <div class="col-md-6 text-left "> Minggu </div>
                      <div class="col-md-6 text-right text-warning"> 09:00 - 19:00 </div>
                    </div>
                </li>
            </ul>
            
            </div>
        </div>


    </div>

    <footer class="section-footer bg-dark">
        <div class="container pt-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-3 align-items-start pb-3 mt-3">
                            <img src="{{asset('asset/logo.png')}}"
                                width="100">
                        </div>
                        <div class="col-12 col-lg-3 align-items-start pb-3" style="font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;">
                        
                        <ul class="list-unstyled text-white p-2">
                            <li>
                                <h4> Hubungi Kami </h4>
                            </li> <br />
                            <li> <small> DISTRICT 8 Building Treasury Tower
                                    36th & 37th Floor SCBD Lot 28
                                    Jl. Jend Sudirman Kav. 52 – 53 Jakarta 12190 
                                </small>
                            </li><br />
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#f0ad4e" class="bi bi-envelope" viewBox="0 0 16 16">
                                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1zm13 2.383-4.708 2.825L15 11.105zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741M1 11.105l4.708-2.897L1 5.383z"/>
                            </svg> <small> dear@erha.co.id <a href=""></a></small>
                            </li><br />
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#f0ad4e" class="bi bi-telephone" viewBox="0 0 16 16">
                                <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.6 17.6 0 0 0 4.168 6.608 17.6 17.6 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.68.68 0 0 0-.58-.122l-2.19.547a1.75 1.75 0 0 1-1.657-.459L5.482 8.062a1.75 1.75 0 0 1-.46-1.657l.548-2.19a.68.68 0 0 0-.122-.58zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.68.68 0 0 0 .178.643l2.457 2.457a.68.68 0 0 0 .644.178l2.189-.547a1.75 1.75 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.6 18.6 0 0 1-7.01-4.42 18.6 18.6 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877z"/>
                            </svg> <small> (021) 5092 2121 </small>
                            </li><br />
                            <li>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#f0ad4e" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                <path d="M13.601 2.326A7.85 7.85 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.9 7.9 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.9 7.9 0 0 0 13.6 2.326zM7.994 14.521a6.6 6.6 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.56 6.56 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592m3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.73.73 0 0 0-.529.247c-.182.198-.691.677-.691 1.654s.71 1.916.81 2.049c.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232"/>
                            </svg> <small> +62-811-2121-2121 </small>
                            </li>
                        <ul>
                       
                        </div>
                        <div class="col-12 col-lg-3 align-items-start pb-3">
                            <h6 class="text-warning font-weight-bold">Ikuti Kami</h6> <br />
                            <span class="p-1 mt-5">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#fff" class="bi bi-facebook" viewBox="0 0 16 16" style="cursor:pointer;" onclick="window.location.href='https://www.facebook.com/erhadermatology.official'">
                                    <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951"/>
                                </svg>
                            </span>
                            <span class="p-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#fff" class="bi bi-instagram" viewBox="0 0 16 16" style="cursor:pointer;" onclick="window.location.href='https://www.instagram.com/erha.ultimate/'">
                                <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.9 3.9 0 0 0-1.417.923A3.9 3.9 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.9 3.9 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.9 3.9 0 0 0-.923-1.417A3.9 3.9 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599s.453.546.598.92c.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.5 2.5 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.5 2.5 0 0 1-.92-.598 2.5 2.5 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233s.008-2.388.046-3.231c.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92s.546-.453.92-.598c.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92m-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217m0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334"/>
                            </svg>
                            </span>
                            <span class="p-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#fff" class="bi bi-youtube" viewBox="0 0 16 16" style="cursor:pointer;" onclick="window.location.href='https://www.youtube.com/@dearerha'">
                                <path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.01 2.01 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.01 2.01 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31 31 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.01 2.01 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A100 100 0 0 1 7.858 2zM6.4 5.209v4.818l4.157-2.408z"/>
                            </svg>
                            </span>
                            <span class="p-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#fff" class="bi bi-tiktok" viewBox="0 0 16 16" style="cursor:pointer;" onclick="window.location.href='https://www.tiktok.com/@erha.ultimate'">
                                <path d="M9 0h1.98c.144.715.54 1.617 1.235 2.512C12.895 3.389 13.797 4 15 4v2c-1.753 0-3.07-.814-4-1.829V11a5 5 0 1 1-5-5v2a3 3 0 1 0 3 3z"/>
                            </svg>
                            </span>

                            <h6 class="text-warning font-weight-bold mt-4">Kebijakan Privasi</h6> 
                        </div>
                        <div class="col-12 col-lg-3 align-items-start bg-transparent">
                            <div class="bg-dark " style="mix-blend-mode:lighten;">
                                <div class="row">
                                    <div class="col-md-6 align-items-start">
                                        <img src="{{asset('asset/footer-phone.jpg')}}" width="130" />
                                    </div>
                                    <div class="col-md-6 align-items-start">
                                     <h5 class="text-white"> ERHA Buddy </h5>
                                     <h6 class="text-warning"> Download </h6>
                                     <p> 
                                        <a href="https://play.google.com/store/apps/details?id=id.erha.userapp&pcampaignid=web_share"><img src="https://d3sgbq9gctgf5o.cloudfront.net/footers/playstore.png?format=auto" width="100" /></a>
                                    </p>
                                    <p> 
                                        <a href="https://apps.apple.com/id/app/erha-buddy/id1507965731?l=id"><img src="https://d3sgbq9gctgf5o.cloudfront.net/footers/appstore.png?format=auto" width="100" /></a>
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </footer>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>

    <script>

    function munculPopUp(){
       document.getElementById("popups").style='display:block';
    }

    function closePopUp(){
       document.getElementById("popups").style='display:none';
    }

    </script>

</html>