@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard Foto') }}</div>
                <div class="card-body">
                    <p> <a href="/foto/add" class="btn btn-primary"> Import JSON File </a> | URL API : http://206.189.152.107/api/foto </p>
                    <table class="table">
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Path</td>
                            <td>Delete</td>
                        </tr>
                        @foreach($data as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->path}}</td>
                            <td><a href="/foto/delete/{{$row->id}}" class="btn btn-danger">Delete</a></td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $data->links(); }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
