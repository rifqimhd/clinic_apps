@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard Foto') }}</div>
                <div class="card-body">
                    <h3>  Add  Data From JSON File : </h3>
                    <h4><a href="{{url('data/sample_foto.json')}}" download>Download Sample Data Foto</a></h4>
                    <form method="POST" action="/foto/store" enctype="multipart/form-data">
                    @csrf
                        <p>
                            <input type="file" name="data" class="form-control" /> 
                        </p>
                        <input type="submit" value="Simpan" class="btn btn-primary" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
